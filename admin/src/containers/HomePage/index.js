import React, { memo, useState, useEffect } from 'react';
import { Button, InputText, Label, Select } from '@buffetjs/core'
import PropTypes from 'prop-types';
import { request } from 'strapi-helper-plugin'
import pluginId from '../../pluginId';

const HomePage = () => {
  const [vnpPaymentGateway, setGateway] = useState('');
  const [vnpTmnCode, setTmnCode] = useState('');
  const [vnpVersion, setVersion] = useState('');
  const [vnpLocale, setLocale] = useState('');
  const [vnpHashSecret, setSecret] = useState('');
  const [vnpReturnUrl, setReturnUrl] = useState('');
  const localeOptions = ['vn', 'en']
  useEffect(() => {
    strapi.lockApp()
    async function getConfig() {
      const configResult = await request(`/${pluginId}`, {
        method: 'GET'
      })
      setGateway(configResult.paymentGateway)
      setTmnCode(configResult.vnp_TmnCode)
      setVersion(configResult.vnp_Version)
      setLocale(configResult.vnp_Locale)
      setSecret(configResult.vnp_HashSecret)
      setReturnUrl(configResult.vnp_ReturnUrl)
    }
    getConfig()
    strapi.unlockApp()
  }, [])
  const updateConfigVnPay = async () => {
    await request(`/${pluginId}/`, {
      method: 'POST',
      body: {
          paymentGateway: vnpPaymentGateway,
          vnp_TmnCode: vnpTmnCode,
          vnp_Version: vnpVersion,
          vnp_Locale: vnpLocale,
          vnp_HashSecret: vnpHashSecret,
          vnp_ReturnUrl: vnpReturnUrl
        }
    })
    strapi.notification.toggle({
      type: 'success',
      message: 'Đã cập nhật cấu hình',
      timeout: 4000,
    })
    setGateway(vnpPaymentGateway)
    setTmnCode(vnpTmnCode)
    setVersion(vnpVersion)
    setLocale(vnpLocale)
    setSecret(vnpHashSecret)
    setReturnUrl(vnpReturnUrl)
  }

  return (
    <div className="container-fluid" style={{ marginTop: '20px' }}>
      <h1>Tích hợp thanh toán VnPay</h1>
      <Label
        htmlFor="vnp_payment_gateway"
        message={`Địa chỉ cổng thanh toán VnPay - Test http://sandbox.vnpayment.vn/paymentv2/vpcpay.html`}
      />
      <InputText
        name="vnp_payment_gateway"
        onChange={({ target: { value } }) => {
          setGateway(value)
        }}
        placeholder="http://sandbox.vnpayment.vn/paymentv2/vpcpay.html"
        type="text"
        value={vnpPaymentGateway || ''}
      />
      <br />
      <Label
        htmlFor="vnp_tmn_code"
        message={`Mã merchant`}
      />
      <InputText
        name="vnp_tmn_code"
        onChange={({ target: { value } }) => {
          setTmnCode(value)
        }}
        placeholder="2QXUI4J4"
        type="text"
        value={vnpTmnCode || ''}
      />
      <br />
      <Label
        htmlFor="vnp_version"
        message={`Phiên bản API mà merchant kết nối. Phiên bản hiện tại: 2.0.1 và 2.1.0`}
      />
      <InputText
        name="vnp_version"
        onChange={({ target: { value } }) => {
          setVersion(value)
        }}
        placeholder="2.0.1 hoặc 2.1.0"
        type="text"
        value={vnpVersion || ''}
      />
      <br />
      <Label
        htmlFor="vnp_locale"
        message={`Chọn ngôn ngữ giao diện hiển thị. Hỗ trợ Tiếng Việt (vn) và Tiếng Anh (en)`}
      />
      <Select
        name="vnp_locale"
        onChange={({ target: { value } }) => {
          setLocale(value);
        }}
        options={localeOptions}
        value={vnpLocale || 'vn'}
      />
      <br />
      <Label
        htmlFor="vnp_secret"
        message={`Chuỗi bí mật`}
      />
      <InputText
        name="vnp_secret"
        onChange={({ target: { value } }) => {
          setSecret(value)
        }}
        placeholder=""
        type="text"
        value={vnpHashSecret || ''}
      />
      <br />
      <Label
        htmlFor="vnp_returnUrl"
        message={`Địa chỉ URL chuyển hướng (URL trang web):`}
      />
      <InputText
        name="vnp_returnUrl"
        onChange={({ target: { value } }) => {
          setReturnUrl(value)
        }}
        placeholder=""
        type="text"
        value={vnpReturnUrl || ''}
      />
      <Button
        color='primary'
        className="primary"
        onClick={updateConfigVnPay}
        style={{ marginTop: '20px' }}
      >
        Lưu
      </Button>
    </div>
  );
};

export default memo(HomePage);
