const vnpay = {
    store: () => strapi.plugins.vnpay.services.store
}

async function updateStoreCredentials({ store }) {
    let paymentGateway = false
    let vnp_TmnCode = false
    let vnp_Version = false
    let vnp_Locale = false
    let vnp_ReturnUrl = false
    let vnp_HashSecret = false

    const { plugins } = strapi.config
    if (plugins && plugins.vnpay) {
        paymentGateway = plugins.vnpay.paymentGateway
        vnp_TmnCode = plugins.vnpay.vnp_TmnCode
        vnp_Version = plugins.vnpay.vnp_Version
        vnp_Locale = plugins.vnpay.vnp_Locale
        vnp_ReturnUrl = plugins.vnpay.vnp_ReturnUrl
        vnp_HashSecret = plugins.vnpay.vnp_HashSecret
    }
    console.log(paymentGateway)
    if (paymentGateway) {
        await store.set({
            key: 'vnpay_gateway',
            value: paymentGateway,
        })
    }


    if (vnp_TmnCode) {
        await store.set({
            key: 'vnpay_tmncode',
            value: vnp_TmnCode,
        })
    }


    if (vnp_Version) {
        await store.set({
            key: 'vnpay_version',
            value: vnp_Version,
        })
    }

    if (vnp_Locale) {
        await store.set({
            key: 'vnpay_locale',
            value: vnp_Locale,
        })
    }

    if (vnp_ReturnUrl) {
        await store.set({
            key: 'vnp_returnurl',
            value: vnp_ReturnUrl,
        })
    }

    if (vnp_HashSecret) {
        await store.set({
            key: 'vnp_hashsecret',
            value: vnp_HashSecret,
        })
    }
}

module.exports = async () => {
    const store = strapi.store({
        environment: strapi.config.environment,
        type: 'plugin',
        name: 'vnpay',
    })
    strapi.plugins.vnpay.store = store

    // initialize credentials from config file
    await updateStoreCredentials({ store })

    // initialize hooks -- no need yet
}