# VNPay Strapi Plugin

Package này giúp tích hợp VNPay với Strapi.

## Mục lục
- [VNPay Strapi Plugin](#vnpay-strapi-plugin)
  - [Mục lục](#mục-lục)
    - [Cài đặt](#cài-đặt)
    - [Cấu hình](#cấu-hình)
    - [Sử dụng](#sử-dụng)
    - [Mở rộng](#mở-rộng)

### Cài đặt
```npm install --save strapi-plugin-vnpay```
Hoặc
```yarn add strapi-plugin-vnpay```

### Cấu hình

### Sử dụng

### Mở rộng