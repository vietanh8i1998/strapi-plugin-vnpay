'use strict';
const _ = require('lodash');
const vnp = require('../helpers/checkout');
/**
 * vnpay-integration.js controller
 *
 * @description: A set of functions called "actions" of the `vnpay-integration` plugin.
 */
const vnpay = {
  store: () => strapi.plugins.vnpay.services.store
}
module.exports = {
  async checkout(ctx){
    console.log(ctx.request)
    const config = await this.getStoreConfig()
    let vnp_IpAddr = ctx.request.body.vnp_IpAddr;
    let vnp_OrderInfo = ctx.request.body.vnp_OrderInfo;
    let vnp_Amount = ctx.request.body.vnp_Amount;
    let vnp_OrderType = ctx.request.body.vnp_OrderType;
    let url = await vnp.buildCheckoutUrl(config, {vnp_IpAddr, vnp_OrderInfo, vnp_Amount, vnp_OrderType})
    ctx.send({url})
  },

  async vnpIpn(ctx){
    //custom logic
    console.log(ctx)
    ctx.send({message: 'ok'})
  },

  /**
   * Get config defined in the admin panel
   */
  async getStoreConfig(ctx){
    const store = await vnpay.store();
    const paymentGateway = await store.getStoreKey('vnpay_gateway')
    const vnp_TmnCode = await store.getStoreKey('vnpay_tmncode')
    const vnp_Version = await store.getStoreKey('vnpay_version')
    const vnp_Locale = await store.getStoreKey('vnpay_locale')
    const vnp_ReturnUrl = await store.getStoreKey('vnp_returnurl')
    const vnp_HashSecret = await store.getStoreKey('vnp_hashsecret')
    return {
      paymentGateway,
      vnp_TmnCode,
      vnp_Version,
      vnp_Locale,
      vnp_ReturnUrl,
      vnp_HashSecret
    }
  },

  async setStoreConfig(ctx) {
    let paymentGateway = ctx.request.body.paymentGateway;
    let vnp_TmnCode = ctx.request.body.vnp_TmnCode;
    let vnp_Version = ctx.request.body.vnp_Version;
    let vnp_Locale = ctx.request.body.vnp_Locale;
    let vnp_HashSecret = ctx.request.body.vnp_HashSecret;
    let vnp_ReturnUrl = ctx.request.body.vnp_ReturnUrl;
    let store = await vnpay.store();
    await store.setStoreKey('vnpay_gateway', paymentGateway);
    await store.setStoreKey('vnpay_tmncode', vnp_TmnCode);
    await store.setStoreKey('vnpay_version', vnp_Version);
    await store.setStoreKey('vnpay_locale', vnp_Locale);
    await store.setStoreKey('vnp_returnurl', vnp_ReturnUrl);
    await store.setStoreKey('vnp_hashsecret', vnp_HashSecret);

    return {
      paymentGateway,
      vnp_TmnCode,
      vnp_Version,
      vnp_Locale,
      vnp_HashSecret,
      vnp_ReturnUrl
    }
  },

};
