'use strict'

/**
 * vnpay service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

async function getStoreKey(key) {

    return this.store().get({ key })
}

async function setStoreKey(key, value) {
    return await this.store().set({
        key: key,
        value: value
    })
}

function store() {
    return strapi.plugins.vnpay.store
}

module.exports = {
    store,
    getStoreKey,
    setStoreKey,
}