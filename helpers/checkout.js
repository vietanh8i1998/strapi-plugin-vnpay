function sortObject(obj) {
    var sorted = {};
    var str = [];
    var key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            str.push(encodeURIComponent(key));
        }
    }
    str.sort();
    for (key = 0; key < str.length; key++) {
        sorted[str[key]] = encodeURIComponent(obj[str[key]]).replace(/%20/g, "+");
    }
    return sorted;
}

function signData(params, secret) {
    // strapi.debug.log(hmac)
    let sign = queryString.stringify(params, { encode: false });
    console.log('Sign', sign)
    console.log('Secret', secret)
    let hmac = crypto.createHmac('sha512', secret);

    var signed = hmac.update(Buffer.from(sign, 'utf-8')).digest("hex");
    params.vnp_SecureHash = signed;
    return params;
}

module.exports = {
    /**
     * Build Checkout URL for VNPay
     *  @param {Object}   config - Config object
     *  @param {Object}   data
     *  @param {String} data.vnp_TxnRef - A unique reference for the transaction (by day), if not provided, the date string with format yyyyMMddHHmmss + Math.floor(Math.random() * 10000) will be used by default.
     *  @param {String} data.vnp_IpAddr - IP address of the client
     *  @param {String} data.vnp_OrderInfo - Order information - No vietnamese characters allowed
     *  @param {Number} data.vnp_Amount - Amount of the transaction
     * @description: Build a checkout URL for VNPay which can then be used to redirect the user to the VNPay payment page.
     * @return {String} The payment url
     */
    buildCheckoutUrl: function(config, data){
        let dateFormat = require("dateformat")
        let date = new Date();
        var createDate = dateFormat(date, 'yyyymmddHHmmss');
        var orderId = dateFormat(date, 'HHmmss');
        var amount = data.vnp_Amount;
        var orderInfo = data.vnp_OrderInfo;
        var orderType = data.vnp_OrderType;
        let vnp_Params = {}
        vnp_Params['vnp_Version'] = '2.1.0';
        vnp_Params['vnp_Command'] = 'pay';
        vnp_Params['vnp_TmnCode'] = config.vnp_TmnCode;
        vnp_Params['vnp_Locale'] = config.vnp_Locale;
        vnp_Params['vnp_CurrCode'] = 'VND';
        vnp_Params['vnp_TxnRef'] = orderId;
        vnp_Params['vnp_OrderInfo'] = orderInfo;
        vnp_Params['vnp_OrderType'] = orderType;
        vnp_Params['vnp_Amount'] = amount * 100;
        vnp_Params['vnp_ReturnUrl'] = config.vnp_ReturnUrl;
        vnp_Params['vnp_IpAddr'] = data.vnp_IpAddr;
        vnp_Params['vnp_CreateDate'] = createDate;
        vnp_Params = sortObject(vnp_Params);
        var querystring = require('qs');
        var signData = querystring.stringify(vnp_Params, { encode: false });
        var crypto = require("crypto");
        var hmac = crypto.createHmac("sha512", config.vnp_HashSecret);
        var signed = hmac.update(new Buffer(signData, 'utf-8')).digest("hex");
        vnp_Params['vnp_SecureHash'] = signed;
        let vnpUrl = config.paymentGateway + '?' + querystring.stringify(vnp_Params, { encode: false });
        return vnpUrl;
    }
}



